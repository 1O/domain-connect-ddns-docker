#!/bin/sh

set -xeu

BUILDX_VERSION=v0.7.1
BUILDX_ARCH=linux-amd64

docker run -d --name dind --privileged -e BUILDX_VERSION=v0.7.1 -e BUILDX_ARCH=linux-amd64 -v $(pwd):/data docker:dind --experimental

docker exec dind sh -c "apk add curl"
docker exec dind sh -c "mkdir -p ~/.docker/cli-plugins"
docker exec dind sh -c "curl -sSLo ~/.docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/$BUILDX_VERSION/buildx-$BUILDX_VERSION.$BUILDX_ARCH"
docker exec dind sh -c "chmod +x ~/.docker/cli-plugins/docker-buildx"
docker exec dind sh -c "docker run --rm --privileged multiarch/qemu-user-static --reset -p yes"
docker exec dind sh -c "docker context create my-context"
docker exec dind sh -c "docker buildx create --use my-context"
docker exec dind sh -c "docker info"

docker exec dind sh -c "docker buildx build --progress plain --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --tag dcdns ."