# Domain Connect DDNS Docker image

This is a simple Docker image which can be used to run the domain connect python application without a local python installation needed.

## How to use

### Initialiaztion

First you need to make sure you initialized a `settings.txt` file. To do so run 
```sh
docker run --rm -v $(pwd):/data ruceforce/dcddns setup --domain example.com
```

For more examples see the official repository <https://github.com/Domain-Connect/DomainConnectDDNS-Python>

### Update DNS entry

After the initialization process a file called `settings.txt` should have been created. To update the DNS entries of your domain you need to mount that file and run the container like this (assuming `settings.txt` lives in your current working directory)

```sh
docker run --rm -v $(pwd)/settings.txt:/data/settings.txt bruceforce/dcddns update --all
```