FROM python:3.10.1-slim
ARG TARGETPLATFORM

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install requirements
RUN if [ "${TARGETPLATFORM}" = 'linux/arm/v7' ]; then apt-get update && apt-get install -y \
        build-essential \
        libssl-dev \
        libffi-dev \
        python3-dev \
        cargo \
    && rm -rf /var/lib/apt/lists/*; \
    fi
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY entrypoint.sh /usr/local/bin

# Create data directory
RUN mkdir /data
WORKDIR /data

ENTRYPOINT [ "entrypoint.sh" ]
